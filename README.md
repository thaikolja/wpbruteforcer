# WPBruteforcer

The **WPBruteforcer** Python 3 class performs a brute force attack on a WordPress site using a user-provided site URL and a password file. It automatically retrieves users' login names from the site's RESTful API and checks user accounts' vulnerabilities by testing various combinations of identified usernames and passwords. It uses [WPScan v3.8.25](https://github.com/wpscanteam/wpscan) under the hood.

This class has been developed and tested on macOS Sonoma (v14.4.1) only.

**Important:** WordPress' RESTful API must be activated and expose the `/wp-json/wp/v2/users` endpoint.

## Features

- Brute force attack on a WordPress site
- Automatical user enumeration via the site's JSON API
- Define custom password lists
- Define how many users should be attacked
- Option to update the WPScan database on the fly (recommended)
- Various class attributes to customize the attack

## Installation

WPBruteforcer is a Python 3 class based on WPScan CLI, which requires Ruby. Make sure to have the following installed:

1. Python 3
2. [Ruby](https://www.ruby-lang.org/en/downloads/) (or install via `brew install ruby`)
3. WPScan (install via `gem install wpscan`)
2. `requests` library (install via `pip install requests`)

## Usage

To use the WPBruteforcer class, initialize it with the URL of the target website. Make sure to use the correct protocol (`http`/`https`).

```python
WPBruteforcer(site_url)
```

## Arguments

- `url`: *string* (required): The URL of the WordPress site to be attacked

## Class Attributes

- `url`: *string* (required) The URL of the WordPress site to be attacked
- `args`: *dict* (optional) Named parameters used for setting additional config:
    - `login_url`: *string* The login URL for performing the attack (e.g., `https://wp.test`)
    - `passwords_file`: *string* Path to the password file (defaults to `./passwords/1000.txt`)
    - `max_users`: *int* The maximum number of users to enumerate (defaults to `5`)
    - `log`: *bool* Enable or disable command log messages (defaults to `True`)
    - `update`: *bool* Update the WPScan database or not (defaults to `True`)

## Examples

### 1. Minimal usage

```python
WPBruteforcer('https://wp.test')
```

### 2. With class attributes

```python
WPBruteforcer(
    url=site_url,
    passwords_file='./passwords/1000.txt',
    max_users=5,
    log=True,
    update=True
)
```

## Credits

* Kolja Nolte <[kolja.nolte@gmail.com](mailto:kolja.nolte@gmail.com)>

## License

MIT

## Legal Notice

This tool should be used for educational and security testing purposes only, as unauthorized brute force attacks are illegal and punishable.
